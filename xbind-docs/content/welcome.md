---
latinum_slug: "welcome"
latinum_layout: "landing"
welcome_page : true
welcome_title : "xBind Docs"
welcome_subtitle : "This is the reference manual for xBind."
title : Home
---

This is the documentation for the core xBind platform. Extensions built on top of this platform
for the purpose of integration with other systems and libraries have also been included in this
documentation.

Note: The pages below are in no particular order, and you can skip to any specific section of your
choice.
