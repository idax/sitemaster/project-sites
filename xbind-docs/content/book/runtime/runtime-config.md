---
latinum_slug : "runtime-config"
title : "Runtime Configuration"
summary: "customizing the behavior of managed entities using data from external sources."
---