---
latinum_slug : "named-injection"
title : "Named Injections"
summary: "Resolving dependency conflicts arising out of duplicate managed entities."
---

# Here Lies the Conflict

So far, the mechanisms of dependency injection discussed revolves around the declared types (of class variable or method parameters) at a point of injection. Instances of managed entities that implement such injection types are chosen by the Crossbinder runtime to resolve the corresponding dependency. This could be a problem if the same injection type is implemented by more than one singleton, prototype or provided object within the scope of a given application. To illustrate,

```java
public interface Shape { }

@Prototype
public class Circle implements Shape { }

@Prototype
public class Square implements Shape { }

@Singleton
public class DrawingBoard {

  @Inject
  private Shape shp;
}
```

The Crossbinder runtime is unable decide between ```Circle``` and ```Square``` for injection into the ```she``` variable. Because of this conflict in dependency resolution the runtime will fail to start and raise an exception condition.

# Naming of Managed Entities

The first step in solving for such conflicts in dependency resolution, is to assign a unique identifier to each type of managed entity (singleton, prototype and provided objects) as a string value. For singletons and prototypes, the required syntax is to provide the identifier as the default value of the corresponding annotation applied on the class definition.

```java
@Prototype("circle")
public class Circle implements Shape { }

@Singleton("square")
public class Square implements Shape { }
```

For providers, the identifier is given as the default value of the ```@Provides``` annotation applied on a provider method.

```java

@Provider
public class ShapeProvider {

  @Provides("triangle")
  public Shape createTriangle() {
    //...
  }
}
```

# Naming Rules

A name can be assigned only to a managed entity that can be injected. Singletons, prototypes and provided objects can be associated with a name. Providers and interceptors cannot be identified using this mechanism.

A name given to a managed entity must be unique across all singletons, prototypes and provided objects within the scope of a Crossbinder runtime. This means that a singleton and a prototype cannot have the same name.

Names are string values with no restrictions on the sequence or choice of characters. Names are case-sensitive.

# Entity Names at Points of Injection

The second step in solving for dependency resolution conflicts, is to specify the unique name of a managed entity at a point of injection. The required syntax is to provide the unique name as the default value to the ```@Inject``` annotation applied on class variables or method parameters. To illustrate,

```java
@Singleton
public class DrawingBoard {

  @Inject("circle")
  private Shape shp1;

  private void applyShape(@Inject("square") Shape shp2) { }

}
```

The Crossbinder runtime will populate ```shp1``` with an instance of ```Circle```, and invoke the ```applyShape``` method with an instance of ```Square```. The dependency resolution, in this case, is based on both the injection type and entity name.

The entity name is an additional input to narrow down to a specific managed entity. It is not mandatory to provide this input even if the corresponding entity has a name, provided that the resolution can be based only on the injection type. For example,

```java
public interface Drawable { }

@Prototype("rectangle")
public class Rectangle implements Drawable { }

@Singleton
public class DrawingBoard {

  @Inject
  private Drawable drw;
}
```

Although no entity name has been specified, ```drw``` will still be injected with an instance of ```Rectangle``` provided there is no other entity that provides an implementation of ```Drawable```.
