---
latinum_slug : "di-mechanism"
title : "Mechanisms of Dependency Injection"
summary: "how dependencies are defined and enforced."
---

# Points of Injection

Dependency injection using Crossbinder occurs when a Java object

* has its class-level variables populated with instances of managed entities, and
* has certain methods invoked with instances of managed entities as parameters.

The managed entities being injected are singletons, prototypes and provider generated objects. Each such class-level variable or method is called a point of injection.

## Injection into Class-Level Variables

The code block below illustrates how a point of injection should be defined on a class-level variable. Note: the example shows injection into a singleton, however this can also be a prototype or a provider.

```java
public interface MyTypeA {}

@Singleton
public class MyManagedEntity {

  @Inject
  private MyTypeA var1;
}
```

Assuming that an implementation exists for ```MyTypeA``` that is a singleton, prototype or provided object, an instance of the same will be set on ```var1``` in an instance of class ```MyManagedEntity```.

The following rules are applicable for injection into class-level variables:

* the class-level variable must be decorated with the ```@Inject``` annotation.
* the variable must be of an interface type. It cannot be the actual implementation type of the singleton, prototype or provided object that will eventually be injected. This is as explained in the code block below:

```java

@Prototype
public class MyProtoA implements MyTypeA {}

@Singleton
public class MyManagedEntity {

  @Inject
  private MyProtoA var1;
  // there will be no injection here, as MyProtoA is a class and not an
  // interface.
}
```

* The visibility of the variable being injected is not relevant. Injection works equally for private and for public variables.

## Injection using Methods

The code block below illustrates how a point of injection should be defined on a method of the class being injected.

```java
public interface MyTypeA {}

public interface MyTypeB {}

@Singleton
public class MyManagedEntity {

  private MyTypeA var1;
  private MyTypeB var2;

  public void doInject1(@Inject MyTypeA v1) {
    var1 = v1;
  }

  public void doInject2(@Inject MyTypeB v2) {
    var2 = v2;
  }

  public void injectAgain(@Inject MyTypeA v1, @Inject MyTypeB v2) {
    var1 = v1;
    var2 = v2;
  }
}
```

Assuming that implementations exist for ```MyTypeA``` and ```MyTypeB``` that are singletons, prototypes or provided objects, Crossbinder will use the the corresponding instances, as appropriate, to invoke methods ```doInject1```, ```doInject2``` and ```injectAgain``` for dependency injection on ```MyManagedEntity```.

The following rules are applicable for injection using methods:

* The method must be public and not static. No special naming convention is required for the method name (e.g. ```getXXX()``` or ```setXXX()```).
* The method return type must be void.
* The method can take any number of parameters.
* All method parameters must be of interface type and must be decorated with the ```@Inject``` annotation.
* The class can expose multiple methods for injection (as per the above rules). All methods will be invoked for dependency injection in no particular order.

Note that a class can expose both class-level variables and methods as points of injection, as shown below:

```java
public interface MyTypeA {}

@Singleton
public class MyManagedEntity {

  @Inject
  private MyTypeA var1;

  public void doInject(@Inject MyTypeA v1) {
    var1 = v1;
  }
}
```

In this case, Crossbinder will directly populate ```var1``` and also invoke the ```doInject``` method, in no special order, with a suitable singleton, prototype or provided object.

# Making Injections Optional

Given the example in the previous section, assume that an implementation of ```MyTypeA``` does not exist as a singleton, prototype or provided entity. In such cases, dependency resolution would fail, and Crossbinder will abort its startup and generate an exception.

In order to avoid such errors, and continue without injecting the dependency, you should decorate the point of injection with the ```@Optional``` annotation. This is as illustrated below for variable injection.

```java
@Singleton
public class MyManagedEntity {

  @Inject @Optional
  private MyTypeA var1;
}
```

After startup, the value of ```var1``` can be null if a singleton, prototype or provided object does not exist that implements ```MyTypeA```. For method injections, the corresponding decoration is as illustrated below.

```java
public interface MyTypeA {}

public interface MyTypeB {}

@Singleton
public class MyManagedEntity {

  private MyTypeA var1;
  private MyTypeB var2;

  public void doInject(
      @Inject @Optional MyTypeA v1,
      @Inject @Optional MyTypeB v2) {
    var1 = v1;
    var2 = v2;
  }
}
```
