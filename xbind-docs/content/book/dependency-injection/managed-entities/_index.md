---
latinum_slug : "managed-entities"
title : "Managed Entities"
summary: "the participants in dependency injection and lifecycle management"
weight: 1
---

# What is a Managed Entity?

A managed entity is a Java class that is instantiated and referenced by xBind and later deferenced
when it is no longer required. The entity can get garbage collected and become unavailable after it
is dereferenced. Additionally, the following capabilities are enabled by xBind for these entities:

* Managed entities can be injected into each other to form a dependency graph as per the
  requirements of your application. xBind ensures that the principles of interface-driven design are
  enforced during such injections.

* At runtime, managed entities can be configured using data stored in external resources, such as
  JSON or XML in files on the local file system.

* Method calls on managed entities can be intercepted in an aspect-oriented manner. This
  interception leads to common behaviour (such as metering, tracing and error handling) to be
  executed around all method calls.

* Execution of custom application logic at pre-defined stages (such as creation, injection and
  dereference) in the lifecycle of managed entities.

# Types of Managed Entities

Entities managed by xBind can be stereotyped based on their durability, cardinality (number of
instances that are active at the same time) and intended usage, into the following:

## Injectable as Dependencies

[Singleton](#!/singleton) : Only one instance of this entity can be present for a xBind runtime.
This is a durable entity that exists so long as the corresponding runtime is available.

[Prototype](#!/prototype) : This is a short lived entity that is instantiated on demand and
subsequently dereferenced (falls out of scope and garbage collected) when no longer needed. Multiple
instances of this entity can be active at the same time for a given xBind runtime.

[Provider](#!/provider) : Acts as a factory for creation of objects that can be injected by xBind
into other managed entities. Only one instance of a provider entity can be present for a given xBind
runtime. However, the provided objects can behave either as singletons or protoypes (in terms of
durability and cardinality) depending on the provider implementation.

## Interceptors

**Lifecycle Interceptor** : Incorporates custom behvaiour that gets executed for each singleton,
prototype and provider instance at defined stages in their lifecycle managed by a xBind runtime.
These stages correspond to: post-creation of the target entity, after injection of dependencies and
when the entity is about to be dereferenced due to runtime termination.

**Method Interceptor** : Incorporates custom behaviour that gets executed every time a method
invocation occurs on a managed entity from the containing entity where it is injected. The
interception points include: before execution, post-execution and the method execution itself
(replacement with a alternate behavior).
