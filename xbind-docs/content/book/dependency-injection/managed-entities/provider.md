---
latinum_slug : "provider"
title : "Providers"
summary: "factories to create and provide objects that are injected as dependencies."
---

# Definition

A provider is a managed entity that serves as a factory to create objects on behalf of the xBind
runtime. These provided objects are then injected as dependencies into other singletons and
prototypes or become the target of lookup operations. Providers are useful for creating and
maintaining objects that cannot be handled by xBind as either singletons or prototypes. This usually
happens when:

1. the provided object is part of an external library/component that is not available for
   source-level modifications, and
2. the target object has an instantiation sequence that cannot be managed by xBind. For example, the
   target object can be a proxy instance or many not have a default constructor.

# Semantics

## The Provider Enity

Providers are plain old Java objects (POJO) that are decorated with the ```@Provider``` annotation.

```java
import id.xbind.dilm.Provider;

@Provider
public class ShapeProvider { }
```

The ```@Provider``` annotation cannot be used in conjunction with ```@Singleton``` or
```@Prototype``` on the same Java class. Doing so would result in an exception being thrown at the
time of runtime startup.

## Factory Methods

For each type of object created and managed by a provider there must exist a method that is capable
of returning such an object. These methods within a provider are called factory methods and are
decorated with the ```@Provides``` annotation, as shown in the example below.

```java
import id.xbind.dilm.Provider;
import id.xbind.dilm.Provides;
import id.xbind.dilm.Singleton;
import id.xbind.dilm.Inject;

public interface Shape {}

public class Circle implements Shape {}

@Provider
public class ShapeProvider {

  @Provides
  public Shape provideCircle() {
    return new Circle();
  }
}

@Singleton
public class DrawingBoard {

  @Inject
  private Shape shp; //this will be injected with an instance of Circle that
  // is obtained by the xBind runtime by making a call to
  //ShapeProvider#provideCircle()
}
```

The following additional semantics must be met by a factory method:

1. There is no additional interpretation associated with the name of the method. Factory methods can
   therefore take any arbitrary names.
2. The method must not be static and must have an access level of public.
3. The method must not take any arguments.
4. The return type of the method must be of an interface type.
5. A provider can have more than one factory methods. However no two factory methods can have the
   same return type.

```java
@Provider
public class MeAProvider {

  /*
   * This is not a factory method as the return type is not of an interface
   * type.
   */
  @Provides
  public MeAnEntity provide1() {
    return new MeAnEntity();
  }

  /*
   * This is not a factory method as the method signature says it is static.
   */
  @Provides
  public static MeAType provide2() {
    return new MeAnEntity();
  }

  /*
   * This is not a factory method as there are one or more method arguments present.
   */
  @Provides
  public MeAType provide4(int val) {
    return new MeAnEntity();
  }

  /*
   * This is not a factory method as there are two such methods with the same
   * return type.
   */
  @Provides
  public MeAType provide5() {
    return new MeAnEntity();
  }

  /*
   * This is not a factory method as there are two such methods with the same
   * return type.
   */
  @Provides
  public MeAType provide6() {
    return new MeAnEntity();
  }
}
```

# Instantiation

The xBind runtime creates and maintains only one instance of a given provider entity at a time.
Instantiation is always done at the time of runtime startup, through invocation of the default
constructor on the provider class. In case such a constructor is not available, the provider will
not be instantiated. If multiple constructors are defined in addition to the default one, xBind will
still use the default constructor to instantiate the provider.

A provider that does not have any factory methods is not instantiated.

Once created, provider entities remain available as long as the corresponding xBind runtime is
active. During this time, providers are subject to lifecycle management as defined and provided by
xBind.

# Provided Objects

Since a provided object is not a managed entity, it is not injected with dependencies or undergo
lifecycle management by the xBind runtime where it is active. xBind does not maintain a runtime
reference to a provided object (the reference being maintained with the provider, instead). Because
of this, a factory method is invoked by the xBind runtime everytime there is a need to obtain the
corresponding provided object for injection into a managed entity.

The nature of a provided object with respect to durability (how long is the object available) and
multiplicity (how many objects of the same type are created) is completely determined by its
provider. This means it is not required of a factory method to return a new provided object
everytime it is invoked. The provider implementation may choose to share the same provided object
across multiple invocations of a factory method.
