---
latinum_slug : "prototype"
title : "Prototypes"
summary: "create new and separate instances, on demand."
---

# Definition

A prototype is a managed entity that is created on demand, at the time of dependency injection into
other entities, or when it is accessed via lookup against a xBind runtime. A new instance of the
prototype is created everytime it is injected or looked up. The instance exists as long as it
referenced in the injected entity or in the calling object and gets garbage collected (like any
other Java object) when it falls out of scope.

# Semantics

Prototypes are plain old Java objects (POJO) that are decorated with the ```@Prototype```
annotation.

```java
import id.xbind.dilm.Prototype;

public interface RoundedShape {
  void render();
}

@Prototype
public class Circle implements RoundedShape {
  //...
}
```

A new instance of this class will be created everytime it is injected into another managed entity.

```java
import id.xbind.dilm.Singleton;
import id.xbind.dilm.Inject;

@Singleton
public class DrawingBoard1 {

  @Inject
  private RoundedShape shape1;
  // this will be injected with a new instance of Circle
}

@Singleton
public class DrawingBoard2 {

  @Inject
  private RoundedShape shape2;
  // this will be injected with a new and different instance of Circle
  // such that shape1 != shape2
}

@Singleton
public class DrawingBoard3 {

  @Inject
  private RoundedShape shape1;

  @Inject
  private RoundedShape shape2;
  // shape1 != shape2, even when they are injected into the same entity.
}
```

Note: the above example shows prototypes being injected into a singleton class. The same example
also holds true for injection into providers and other prototypes.

# Instantiating a Prototype

For xBind to instantiate a class annotated with ```@Prototype```, the following rules apply:

1. the class must define a default (no-arguments) constructor. In case such a constructor is not
   available, the class will not be instantiated. Alternatively, if multiple constructors are
   defined in addition to the default one, xBind will still use the default constructor to
   instantiate the prototype.

2. the class must implement one or  more interface types that can be used to define the type of
   object being injected or looked up against a xBind runtime.

```java
import id.xbind.dilm.Prototype;

@Prototype
public class Ellipsoid { }
```

Although decorated with the ```@Prototype``` annotation, this class will not be treated as a
prototype by xBind as it does not implement any injectable interface.

```java
import id.xbind.dilm.Prototype;

@Prototype
public class Ellipsoid implements RoundedShape {

  public Ellipsoid(int minrad, int maxrad) {}
}
```

This class will be treated as a prototype by xBind, but never instantiated (and therefore injected
or referenced) as it does not have a default constructor.

```java
import id.xbind.dilm.Prototype;

@Prototype
public class Ellipsoid implements RoundedShape {

  public Ellipsoid() {}

  public Ellipsoid(int minrad, int maxrad) {}
}
```

This class can be instantiated as a prototype by xBind. However, the second constructor that takes
two arguments is is effectively dead code. xBind will never use this constructor to create an
instance of this class.

# Durability of Prototypes

Since prototypes are created on demand, and garbage collected once they fall out of scope, it is
natural to assume such entities to be short-lived when compared to the duration of an application.
This is not necessarily true. Based on the dependency graph within you application, a prototype can
end up having a lifetime that is nearly equal to the lifetime of the xBind runtime where it is
deployed. Here are some dependency patterns to illustrate the durability of prototypes.

1. **Prototypes injected into singletons:** In general, a prototype injected into a singleton would
   have its reference held by the singleton throughout the latter's lifetime. Recall that a
   singleton has a lifetime that coincides very nearly with the lifetime of the xBind runtime. This
   would make the injected prototype highly durable.

2. **Prototypes injected into providers:** Like singletons, providers have lifetime that is
   equivalent to the lifetime of the corresponding xBind runtime. Therefore, any prototype that is
   injected into a provider becomes highly durable.

3. **Protoypes that are located:** Locating a prototype, would result in a new instance of the
   prototype being created. This instance would remain valid as long as the locating entity
   maintains a reference to the prototype instance.

4. **Prototypes injected into other prototypes:** A prototype injected into another prototype would
   have the same durability as the containing prototype. This is also true for a multi-level
   dependency chain where the containing prototype is in turn injected into another prototype.
    * If the prototype at the root of the dependency chain is injected into a singleton or a
      provider, the entire chain of prototypes becomes highly durable.
    * If the dependency chain gets created due to the root element being located, then all protoypes
      in the chain remain valid as long as the locating entity maintains a reference to the
	  prototype instance.
