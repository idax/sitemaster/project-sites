---
latinum_slug : "singleton"
title : "Singletons"
summary: "created exactly once, thereafter shared with everyone."
weight: 1
---

# Definition

A singleton is a managed entity that is instantiated only once during the lifecycle of a xBind
runtime. The instance is stored in a cache within the runtime, and all subsequent requests for and
references to that singleton always return the cached object. The instance remains available so long
as the containing runtime is active, and is disposed (via Java garbage collection) only when the
runtime is stopped. Singletons are, therefore, both shared and durable entities.

# Semantics

Singletons are plain old Java objects (POJO) that are decorated with the ```@Singleton```
annotation.

```java
import id.xbind.dilm.Singleton;

public interface Shape {
  void render();
}

@Singleton
public class Square implements Shape {

  @Override
  public void render() { }
}
```

A single instance of the ```Square``` class is created and mapped internally to the ```Shape```
interface. Thereafter any other managed entity that has a dependency on ```Shape``` will be injected
with the global instance of ```Square```.

```java
import id.xbind.dilm.Singleton;
import id.xbind.dilm.Inject;

public interface Surface {
  void draw();
}

@Singleton
public class DrawingBoard1 implements Surface {

  @Inject
  private Shape shape1;
  // this will be injected with an instance of Square
}

@Singleton
public class DrawingBoard2 implements Surface {

  @Inject
  private Shape shape2;
  // after dependency injection shape2 == shape1
}
```

Note that the ```@Inject``` annotation on variables ```shape1``` and ```shape2``` declares the
dependency of ```DrawingBoard1``` and ```DrawingBoard2``` on ```Shape``` as described in
[Mechanisms of Dependency Injection](#!/di-mechanism).

# Instantiation

Instantiation of a singleton is done by the xBind runtime, at the time of startup, through
invocation of the default constructor on the singleton class. In case such a constructor is not
available, the singleton will not be instantiated. If multiple constructors are defined in addition
to the default one, xBind will still use the default constructor to instantiate the singleton.

```java
// This class will never get instantiated as it does not expose a default
// constructor.

@Singleton
public class BlackSquare implements Shape {

  public BlackSquare(int width, int height) {}
}

// This class can get instantiated as it exposes a default constructor.

@Singleton
public class Square implements Shape {

  public Square() {}

  public BlackSquare(int width, int height) {
    // however, this constructor will never be used with xBind
  }
}
```

# Lazy Loading a Singleton

A variation to the above example, is to instantiate a singleton only when it is used for the first
time after being injected into another managed entity or requested for using the runtime APIs. This
could potentially be much later after xBind has started. The behaviour is achieved by further
decorating the singleton class with the ```@Lazy``` annotation.

```java
import id.xbind.dilm.Singleton;
import id.xbind.dilm.Lazy;
import id.xbind.dilm.Inject;

@Singleton @Lazy
public class LateSquare implements Shape {

  @Override
  public void render() {}
}

@Singleton
public class DrawingBoard implements Surface {

  @Inject
  private Shape shape;

  @Override
  public void draw() {
    // the global instance of LateSquare will be created in the next line
    // below.
    shape.render();
  }
}
```

After the runtime has finished loading, an instance of ```DrawingBoard``` is available but not
```LateSquare```. Only when there is a call to ```Surface.draw()``` will an instance of
```LateSquare``` be created and have its ```render()``` method invoked.

# Shadow Singletons

For injection into other managed entities, a singleton must implement at least one suitable
interface that is used to denote the type of the object being injected. A singleton that does not
implement any such interface is called a shadow singleton. Such an entity can be instantiated and
stay active within the scope of a xBind runtime. However, it will not be available to other entities
as it will not participate in dependency injection.

```java
@Singleton
public class DrawingBoard implements Surface { }

//This is a shadow singleton
@Singleton
public class DrawingApp {

  @Inject
  private Surface surface;
}
```

All other aspects of dependency injection and lifecycle management remains the same as that of a
regular singleton. In the above example, once the runtime has finished loading, ```surface``` will
be assigned a suitable instance of ```DrawingBoard```.
