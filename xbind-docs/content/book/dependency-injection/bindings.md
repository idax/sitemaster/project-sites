---
latinum_slug : "bindings"
title : "Bindings"
summary: "Associate interfaces with entities that participate in dependency injection."
---

# About Bindings

Bindings are logical associations that are maintained at runtime by Crossbinder against specific Java interface types. These Java interfaces, also called injectable types, are used in defining [points of dependency injection](#!/di-mechanism). The purpose of having these bindings is to determine and retrieve the corresponding singleton, prototype or provided objects to be used at the time of injection.

For singletons and prototypes that implement one or more injectable types, a separate binding is maintained between each of the injectable types and the singleton or the prototype class.

```java
@Singleton
public class DrawingBoard implements Surface, Container { }

@Prototype
public class Circle implements Shape { }
```

This creates the following bindings:

* interface ```Surface``` to class ```DrawingBoard```
* interface ```Container``` to class ```DrawingBoard```
* interface ```Shape``` to class ```Circle```

For providers, a binding is created between a factory method (within the provider) and the method return type.

```java
// Implementation classes that can get injected into managed entities.
public class Square implements FourSided { }
public class Circle implements Rounded { }

@Provider
public class ShapeProvider {

  @Provides
  public Foursided obtainFoursided() {
    return new Square();
  }

  @Provides
  public Rounded provideRounded() {
    return new Circle();
  }
}
```

This creates the following bindings:

* interface ```Foursided``` to method ```ShapeProvider.obtainFoursided```
* interface ```Rounded``` to method ```ShapeProvider.provideRounded```

# Creating Bindings

Bindings are created behind the scenes by the Crossbinder runtime at the time of startup. It uses a combination of annotations, convention and reflection to determine the entities that constitute a binding. The binding information is lost when the Crossbinder runtime is stopped and gets created once again when the runtime is started (or a new runtime is created).

Note that bindings are created against a given injectable type even when there are no points of injection, within you code, that makes use of the injection type.

# Default Binding Rules

## Singletons and Prototypes

**Rule 1:** A separate binding should exist between a singleton/prototype and each of its implemented interfaces. This has been illustrated in the examples above.

**Rule 2:** If the singleton/prototype has a super-class, a separate binding shall also exist between each of the interfaces implemented by the super-class and the starting singleton/prototype. To illustrate,

```java
public class SuperSingle implements MeAType1, MeAType2 { }

@Singleton
public class MeASingle extends SuperSingle implements MeAType3 { }
```

Bindings will exist for each of the interfaces ```MeAType1```, ```MeAType2``` and ```MeAType3``` with the class ```MeASingle```.


**Rule 3:** No separate binding shall exist with a super-class and any of the interfaces unless the super-class itself is annotated to be a singleton or a prototype. Modifying the above example,

```java
@Singleton
public class SuperSingle implements MeAType1, MeAType2 { }

@Singleton
public class MeASingle extends SuperSingle implements MeAType3 { }
```

Bindings will now exist for each of the interfaces ```MeAType1``` and ```MeAType2``` with the class ```SuperSingle```. This is in addition to bindings created for each of the interfaces ```MeAType1```, ```MeAType2``` and ```MeAType3``` with the class ```MeASingle```.

**Rule 4:** If an interface implemented by a singleton/prototype has one or more super-interfaces, a separate binding shall exist between each of the super-interfaces and the starting singleton/prototype. To illustrate,

```java
public interface MeAType4 extends ParentType1, ParentType2 { }

@Singleton
public class MeASingle extends SuperSingle implements MeAType4 { }
```

Bindings will now exist for each of the interfaces ```ParentType1```, ```ParentType2``` and ```MeAType4``` with the class ```MeASingle```.

**Rule 5:** The above rules are applicable across all levels of inheritence of super-classes and super-interfaces (and not just immediate parent) starting from a given singleton/prototype.

## Providers

**Rule 1:** For each provider, a separate binding should exist between a factory method (of the provider class) and the corresponding interface that is the return type of that method. If the provider derives from a super-class, similar bindings are also created for all factory methods in the super class.

**Rule 2:** There is no traversal of class hierarchy. If a return type inherits a super-interface, no separate binding shall exist between the super interface and the corresponding factory method.


# Controlling Binding Behavior

For singletons and prototypes, unnecessary bindings may get created as per the default creation behavior as described above. This is especially true in case the singleton or prototype implements marker interfaces like ```java.io.Serializable``` that has a very remote chance of being used at a point of injection.

You can control binding behavior in one of the following ways:

* Explicitly state that an interface will always be bound to the entities that implement that interface.
* Explicitly state that an interface will never be bound to the entities that implement that interface.

## The @Bindable Annotation

The ```@Bindable``` annotation can only be used on Java interfaces to mandate that these interfaces be always regarded as injectable types. Once annotated, these interfaces are thereafter called bindable types, leading to the following rules of binding creation:

**Rule 1:** A binding is always created between a bindable interface and any singleton and prototype class that implements that interface.

**Rule 2:** For a given singleton or prototype that implements at least one bindable interface, no additional bindings will be created as per default binding rules.

To illustrate:

```java
public interface Shape { }

@Bindable
public interface Shape2D { }

@Prototype
public class Circle implements Shape, Shape2D { }
```

A binding will be created between ```Shape2D``` and ```Circle```, but not between ```Shape``` and ```Circle```.

**Rule 3:** In case a singleton or prototype implements multiple interfaces, and only some of them are bindable, bindings will exist only for the bindable types. To illustrate,

```java
public interface Shape { }

@Bindable
public interface Shape2D { }

@Bindable
public interface Rounded { }

@Prototype
public class Circle implements Shape, Shape2D, Rounded { }
```

Bindings will be created between ```Shape2D``` and ```Circle```, and between ```Rounded``` and ```Circle```. No binding will be created between ```Shape``` and ```Circle```.

**Rule 4:** The above rules are also applicable for any super-class of a singleton or prototype and the bindable types they implement. To illustrate,

```java
public interface Shape { }

public interface Drawable { }

@Bindable
public interface Shape2D { }

@Bindable
public interface Drawable2D { }

public class Circle implements Shape, Shape2D { }

@Singleton
public class Ellipse extends Circle implements Drawable, Drawable2D { }
```

This would create separate bindings with each of ```Shape2D``` and ```Drawable2D``` with ```Ellipse```.

## The @NonBindable Annotation

The ```@NonBindable``` annotation can only be used on Java interfaces to mandate that these interfaces be always regarded as types that are never injectable. The following rules of binding creation are applicable:

**Rule 1:** For a given singleton or prototype that implements multiple interfaces, some of which are non bindable and none are bindable, bindings are created with interfaces that are not decorated with the ```@NonBindable``` annotation. To illustrate,

```java
public interface Shape { }

public interface Drawable { }

@NonBindable
public interface Rounded { }

@Prototype
public class Circle implements Shape, Drawable, Rounded { }
```

This would create separate bindings with each of ```Shape``` and ```Drawable``` with ```Circle```. No binding will be created between ```Rounded``` and ```Circle```.

**Rule 2:** For a given singleton or prototype that implements multiple interfaces, some of which are non bindable and some are bindable, bindings are created only for the bindable types. To illustrate,

```java
@Bindable
public interface Shape { }

public interface Drawable { }

@NonBindable
public interface Rounded { }

@Prototype
public class Circle implements Shape, Drawable, Rounded { }
```

A binding will be created between ```Shape``` and ```Circle```. No such binding will exist for each of ```Drawable``` and ```Rounded``` with ```Circle```.

**Rule 3:** The above rules are also applicable for any super-class of a singleton or prototype and the non bindable types they implement. To illustrate,

```java
@NonBindable
public interface Shape { }

@NonBindable
public interface Drawable { }

public interface Shape2D { }

public interface Drawable2D { }

public class Circle implements Shape, Shape2D { }

@Singleton
public class Ellipse extends Circle implements Drawable, Drawable2D { }
```

This would create separate bindings with each of ```Shape2D``` and ```Drawable2D``` with ```Ellipse```.
