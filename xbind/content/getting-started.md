---
latinum_slug: "getting-started"
title : Getting Started
---

# Project Structure

In order to have better modularity, xBind is comprised of multiple projects, as described below.
Each project has its own binary distribution that can be selectively included in your application.

| Project Name            . | Description |
| ------------ | ----------- |
| xbind-dilm | this is the core runtime for dependency injection and the lifecycle management of Java objects. |
| xbind-commons-config | provides integration with Apache Commons to configure managed entities using runtime information from external locations. Using Apache Commons provides the flexibility to store the configuration data in a variety of formats and to load the same from more than one local/remote location. |
| xbind-servlet | lets a xBind runtime, along with managed entities, be launched within a standard servlet environment. Any servlet within the same environment can then be injected with the entities managed by xBind. |
| xbind-extern | allows objects from an external scope to be injected into entities (such as singletons and prototypes) that are managed by xBind. |
| xbind-jackson | provides integration with Jackson to configure managed entities using runtime information stored in JSON and YAML files. |

The rest of this page describes various mechanisms to integrate only the core runtime (xbind-dilm)
with your project binaries.

# Binary Distribution

The binaries for ```xbind-dilm``` and related sub-projects, are hosted on, and made available
through a Maven repository. You can browse the contents of this repository from
https://www.idax.me/maven-repo/.

## Maven Build

If your project is using Maven as the build management tool, you need to make the following changes
to the project object model definition (pom.xml).

1. Explicitly specify the repositories to use. This is necessary as the xBind binaries are hosted
from a Web location that is outside of the Maven Repository Switchboard (the default repository in
case none is specified in the POM).
2. Add the dependencies on ```xbind-dilm``` with the appropriate version number.

These are as shown in the POM code fragment below:

```xml
<repositories>
  <!-- repository location for xBind binaries -->

  <repository>
    <snapshots>
      <enabled>false</enabled>
    </snapshots>
    <id>idax-release</id>
    <url>https://maven.idax.me/repo/libs-release</url>
  </repository>


  <repository>
    <id>idax-snapshot</id>
    <url>https://maven.idax.me/repo/libs-snapshot</url>
    <snapshots>
      <enabled>true</enabled>
    </snapshots>
  </repository>

  <!--
  The default Maven repository must also be defined to load additional
  binaries, if required by your project.
  -->
  <repository>
    <id>central</id>
    <name>Maven Repository Switchboard</name>
    <layout>default</layout>
    <url>http://repo1.maven.org/maven2</url>
    <snapshots>
      <enabled>false</enabled>
    </snapshots>
  </repository>
</repositories>

<dependencies>
  <dependency>
    <groupId>id.xbind</groupId>
    <artifactId>id.xbind.dilm</artifactId>
    <version>0.6-SNAPSHOT</version>
  </dependency>
</dependencies>
```

## Gradle Build

If Gradle is your choice of build management tool, the following changes needs to be made to the
build script (file ```build.gradle```) of your project.

```java
apply plugin: 'java'

repositories {
  maven {
    url "https://maven.idax.me/repo/libs-release"
  }
  mavenCentral()
}

dependencies {
  compile group: 'id.xbind', name: 'id.xbind.dilm', version: '0.6-SNAPSHOT'
  // other project-specific dependencies
}
```

Similar to the Maven POM example above, you have to explicitly specify the repository for fetching
xBind binaries. You also need to point to the Maven Repository Switchboard for additional,
project-specific binaries that would not be available in the xBind repository.

# Building from Source

The instructions in this, and the following sections, assumes that the developer environment is
running on some variant of the Linux operating system. It is left to the user to create a similar
environment for Windows and Mac OSX systems with little effort.

## Prerequisites

Before you start, ensure that the following is installed on the developer machine:

1. A Java Development Kit (JDK) of version 8.0 and later. You can use the one provided by
[Oracle](https://www.oracle.com/java/), or the [Zulu OpenJDK](http://www.azul.com/downloads/zulu/).
You can also use the OpenJDK distribution that is available from the software repository
corresponding to your Linux environment.
2. [Apache Maven](http://maven.apache.org/) as the build management tool, to generate and install
the binary files on the local system. Maven is usually available as a compressed (zip/tar) archive.
Download the archive and install it such that `mvn` is available on the path for direct invocation.
3. The command-line git client (optional), in case you wish to clone the repository and follow a
custom build process of your choice. Git should be available from the software repository
corresponding to your Linux environment.

## Fetching the Source

The source code for xBind is hosted on Gitlab across the following repositories:

| Project Name            . | Repository Path |
| ------------ | ----------- |
| xbind-dilm   | https://gitlab.com/idax/glu/xbind |

Each release version is tagged, with a snapshot of the source code available under the releases
section of the repository view. Download the snapshot as a zip/tar archive and expand the same into
a build folder on the local file system.

The latest snapshot version is available on the master branch and can be fetched through a git clone
operation or directly downloaded as a
[source code archive](https://gitlab.com/idax/glu/xbind/-/archive/master/xbind-master.zip).

## Included Build Scripts

In order to simplify the build process, we have included a few shell scripts along with the source
code, that will let you create binary packages without explicitly having to issue maven command.
These scripts are available under the ```buildscripts``` folder within the source code archive.

The following scripts are available to create binary artifacts without making any changes to the
source code.

| Script Name         . | Intended Usage |
| ------------ | -------------- |
| build-package.sh | Used to create the xBind jars that are thereafter available in the ```target``` folder under the root of the source code tree on the local machine. |
| build-install.sh | Used to create the xBind jars and copy the same into a maven repository on the local machine. Other local projects can link with these jar files provided they are using Maven/gradle as the build management tool. |

All scripts must be executed from the ```buildscripts``` folder, with the assumption that all
necessary prerequisites (see above) are available on the developer machine.
